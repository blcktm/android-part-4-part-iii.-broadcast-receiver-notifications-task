package com.sourceit.task1.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.NotificationCompat;

import com.sourceit.task1.App;
import com.sourceit.task1.R;
import com.sourceit.task1.utils.L;

/**
 * Created by ${blcktm} on 12.02.2016.
 */
public class MyReceiver extends BroadcastReceiver {

    private Resources res = App.getApp().getResources();
    private final String ANDROID_INTENT_ACTION_AIRPLANE_MODE = res.getString(R.string.action_airplane);
    private final int ID = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        L.d("onReceive");

        NotificationManager nm = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (intent.getAction().equals(ANDROID_INTENT_ACTION_AIRPLANE_MODE)) {
            if (!intent.getBooleanExtra(context.getString(R.string.state), false)) {
                L.d("onReceive false");
                nm.cancel(ID);
            } else {
                L.d("onReceive true");
                Intent notificationIntent = new Intent();
                PendingIntent contentIntent = PendingIntent.getActivity(context,
                        ID, notificationIntent,
                        PendingIntent.FLAG_CANCEL_CURRENT);

                NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
                builder.setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setTicker(context.getString(R.string.note1))
                        .setContentText(context.getString(R.string.note1));

                Notification n = builder.build();
                nm.notify(ID, n);
            }
        }
    }
}
